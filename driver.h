//Final Project - CS120
//Zhen Guo
//Jordan Kirsch
//Ethan Meyers
//  driver.h
//  FP-restaurant
//
//  Created by Zhen Guo on 11/28/18.
//  Copyright © 2018 Zhen. All rights reserved.
//

#ifndef CS120FINALPROJECT_DRIVER_H
#define CS120FINALPROJECT_DRIVER_H


#include <stdexcept>
#include <iostream>
#include <string>

using namespace std;

#include "time.h"
#include "order.h"

class Driver {
public:

    /*Constructor
     *
     * Pre: none
     * Post: Creates a logged-in driver with the given name.
     */
    Driver(const string name);

    /* Copy constructor
     *
     * Pre: none
     * Post: Initializes the driver to be equivalent to the other driver.
     * */
    Driver(const Driver& other);

    /* Overloaded assignment operator
     *
     * Pre: none
     * Post: Overloaded assignment operator. Sets the  Driver to be equivalent to the source
     * object parameter and returns a reference to the modified driver.
     * */
    Driver& operator=(const Driver& other);

    /* Login
     *
     * Pre: Driver is not logged in.
     * Post: Logs the driver in.
     */
    void login() throw(logic_error);

    /* Logout
     *
     * Pre: Driver is logged in and at the restaurant.
     * Post: Logs the driver out.
     */
    void logout() throw(logic_error);

    /* Depart
     *
     * Pre: Driver is logged in and at the restaurant.
     * Post: Driver is delivering. Departure time is recorded.
     */
    void depart(Time time, Order o) throw (logic_error);

    /* Deliver
     *
     * Pre: Driver is delivering, tip >= 0.
     * Post: Driver is not delivering. Driver’s stats are updated.
     */
    void deliver(Time time, float tip) throw (logic_error);

    /* Arrive
     *
     * Pre: Driver is driving but not delivering.
     * Post: Driver is at the restaurant. Driver’s stats are updated.
     */
    void arrive(Time time) throw (logic_error);

    /* Get Name
     *
     * Pre: none
     * Post: Returns the driver’s name.
     */
    string getName() const;

    /* IsLoggedIn
     *
     * Pre: none
     * Post: Returns true if and only if the driver is logged in.
     *       Returns false otherwise.
     */
    bool isLoggedIn() const;

    /* getTotalDeliveries
     *
     * Pre: none
     * Post: Returns the total number of completed deliveries.
     */
    int getTotalDeliveries() const;

    /* getTotalMinDelivering
     *
     * Pre: none
     * Post: Returns the total minutes spent delivering (i.e., between “depart” and “deliver” commands).
     */
    int getTotalMinDelivering() const;

    /* getTotalMinDriving
     *
     * Pre: none
     * Post: Returns the total minutes spent driving (i.e., between “depart” and “arrive” commands).
     */
    int getTotalMinDriving() const;

    /* getTotalTips
     *
     * Pre: none
     * Post: Returns the total tips received, in dollars.
     */
    float getTotalTips() const;

    /* getOrder
     *
     * Pre: Driver is delivering.
     * Post:  Returns the order being delivered.
     */
    Order getOrder() const throw (logic_error);

    /* toString
     *
     * Pre: none
     * Post:  Returns a string containing the driver’s name, state (e.g., not logged in),
     * and, if the driver is delivering an order, the departure time and
     * toString of the order being delivered.
     */
    string toString();

private:
    string name;
    enum states{NOT_LOGGED,AT_RESTAURANT, DELIVERING,DRIVING_BACK};
    states status;
    Time depart_time;
    int deliver_time;
    int drive_time;
    Order order;
    float tips;
    int deliveries;
};

#endif //CS120FINALPROJECT_DRIVER_H
