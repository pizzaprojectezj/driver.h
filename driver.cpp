//Final Project - CS120
//Zhen Guo
//Jordan Kirsch
//Ethan Meyers
//  driver.cpp
//  FP-restaurant
//
//  Created by Zhen Guo on 12/04/18.
//  Copyright © 2018 Zhen. All rights reserved.
//

#ifndef CS120FINALPROJECT_DRIVER_CPP
#define CS120FINALPROJECT_DRIVER_CPP


#include <stdexcept>
#include <iostream>
#include <string>

using namespace std;

#include "Time.h"
#include "Order.h"
#include "Driver.h"


/*Constructor
 *
 * Pre: none
 * Post: Creates a logged-in driver with the given name.
 */
Driver::Driver(const string driver)
{
    name = driver;
    tips = 0;
    deliveries = 0;
    deliver_time = 0;
    drive_time = 0;
    status = AT_RESTAURANT;
}

/* Copy constructor
 *
 * Pre: none
 * Post: Initializes the driver to be equivalent to the other driver.
 * */
Driver :: Driver( const Driver& other )
{
    operator=(other);
}

/* Overloaded assignment operator
 *
 * Pre: none
 * Post: Overloaded assignment operator. Sets the  Driver to be equivalent to the source
 * object parameter and returns a reference to the modified driver.
 * */
Driver& Driver :: operator= ( const Driver& other )
{
    
    if (this != &other) {
        this-> name = other.name;
        this-> status = other.status;
        this-> depart_time = other.depart_time;
        this-> deliver_time = other.deliver_time;
        this-> drive_time = other.drive_time;
        this-> order = other.order;
        this-> tips = other.tips;
        this-> deliveries = other.deliveries;
    }
    return *this;
}


/* Login
 *
 * Pre: Driver is not logged in.
 * Post: Logs the driver in.
 */
void Driver::login() throw(logic_error)
{
    if(status != NOT_LOGGED)
        __throw_logic_error("***Driver is already logged in***");
    status = AT_RESTAURANT;
}

/* Logout
 *
 * Pre: Driver is logged in and at the restaurant.
 * Post: Logs the driver out.
 */
void Driver::logout() throw(logic_error)
{
    if(status != AT_RESTAURANT)
        __throw_logic_error("***Driver has not returned or is not logged in***");
    status = NOT_LOGGED;
}

/* Depart
 *
 * Pre: Driver is logged in and at the restaurant.
 * Post: Driver is delivering. Departure time is recorded.
 */
void Driver::depart(Time time, Order o) throw (logic_error)
{
    if(status != AT_RESTAURANT)
        __throw_logic_error("***Driver is not at restaurant***");
    depart_time = time;
    order = o;
    status = DELIVERING;
    order.depart();
}

/* Deliver
 *
 * Pre: Driver is delivering, tip >= 0.
 * Post: Driver is not delivering. Driver’s stats are updated.
 */
void Driver::deliver(Time time, float tip) throw (logic_error)
{
    if(status != DELIVERING)
        __throw_logic_error("***Driver is not delivering***");
    if(tip < 0)
        __throw_logic_error("***Driver has not received any tip***");
    deliver_time = deliver_time + Time::elapsedMin(depart_time, time);
    order.deliver(time);
    deliveries++;
    tips = tips + tip;
    status = DRIVING_BACK;
}

/* Arrive
 *
 * Pre: Driver is driving but not delivering.
 * Post: Driver is at the restaurant. Driver’s stats are updated.
 */
void Driver::arrive(Time time) throw (logic_error)
{
    if(status != DRIVING_BACK)
        __throw_logic_error("***Driver is not driving back***");
    
    drive_time = drive_time + Time::elapsedMin(depart_time, time);
    status = AT_RESTAURANT;
}

/* Get Name
 *
 * Pre: none
 * Post: Returns the driver’s name.
 */
string Driver::getName() const
{
    return name;
}

/* IsLoggedIn
 *
 * Pre: none
 * Post: Returns true if and only if the driver is logged in.
 *       Returns false otherwise.
 */
bool Driver::isLoggedIn() const
{
    if(status == NOT_LOGGED)
        return false;
    else
        return true;
}

/* getTotalDeliveries
 *
 * Pre: none
 * Post: Returns the total number of completed deliveries.
 */
int Driver::getTotalDeliveries() const
{
    return deliveries;
}

/* getTotalMinDelivering
 *
 * Pre: none
 * Post: Returns the total minutes spent delivering (i.e., between “depart” and “deliver” commands).
 */
int Driver::getTotalMinDelivering() const
{
    return deliver_time;
}

/* getTotalMinDriving
 *
 * Pre: none
 * Post: Returns the total minutes spent driving (i.e., between “depart” and “arrive” commands).
 */
int Driver::getTotalMinDriving() const
{
    return drive_time;
}

/* getTotalTips
 *
 * Pre: none
 * Post: Returns the total tips received, in dollars.
 */
float Driver::getTotalTips() const
{
    return tips;
}

/* getOrder
 *
 * Pre: Driver is delivering.
 * Post:  Returns the order being delivered.
 */
Order Driver::getOrder() const throw (logic_error)
{
    if(status != DELIVERING)
        __throw_logic_error("***Driver is not delivering***");
    return order;
}

/* toString
 *
 * Pre: none
 * Post:  Returns a string containing the driver’s name, state (e.g., not logged in),
 * and, if the driver is delivering an order, the departure time and
 * toString of the order being delivered.
 */
string Driver::toString()
{
    string status_quo;
    switch (status) {
        case NOT_LOGGED:
            status_quo = "not logged in";
            break;
        case AT_RESTAURANT:
            status_quo = "at restaurant";
            break;
        case DELIVERING:
            status_quo = "delivering "
            + depart_time.toString() + ' '
            + order.toString();
            break;
        case DRIVING_BACK:
            status_quo = "driving back";
            break;
    }
    string driver_info = name + ' ' + status_quo;
    
    return driver_info;
}

#endif